%{ -*- Octave -*-  %}


%{ tco-functools - tools to define tail-call-optimized functions and more.  %}
%{ Copyright (C) 2016 Alex Vong  %}

%{ This program is free software: you can redistribute it and/or modify  %}
%{ it under the terms of the GNU General Public License as published by  %}
%{ the Free Software Foundation, either version 3 of the License, or  %}
%{ (at your option) any later version.  %}

%{ This program is distributed in the hope that it will be useful,  %}
%{ but WITHOUT ANY WARRANTY; without even the implied warranty of  %}
%{ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  %}
%{ GNU General Public License for more details.

%{ You should have received a copy of the GNU General Public License  %}
%{ along with this program.  If not, see <http://www.gnu.org/licenses/>.  %}


%{ Perform tail call optimization on tail-recursive function REC_FUNC.  %}
%{ VARARGIN is the argument list used to call REC_FUNC.  %}
function res = wrapper__(rec_func, varargin)
  call = @feval;
  apply = @(func, arg_ls) ...
           func(arg_ls{:});
  Y2 = @(h) ...
        call(@(x) x(x), ...
             @(g) ...
              h(@(varargin) ...
                 @() apply(g(g), varargin)));
  is_procedure = @(x) ...
                  isa(x, 'function_handle');
  out = apply(Y2(rec_func), varargin);

  while (is_procedure(out))
    out = out();
  end
  res = out;

end
